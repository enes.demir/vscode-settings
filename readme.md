# LİNUX

> Sen zaten ne yapacağını biliyorsun ;)
## Projeyi localinize almak için

    mkdir vscode
    cd vscode
    git clone git clone git@gitlab.com:enes.demir/user-settings.git

# WİNDOWS

## Projeyi localinize almak için

    mkdir vscode
    cd vscode
    git clone git clone git@gitlab.com:enes.demir/user-settings.git

## Yeni bir depo oluştur

> SSH Key tanımlı ise

    git clone git@gitlab.com:enes.demir/user-settings.git

> SSH Key tanımlı değil ise`

    https://gitlab.com/enes.demir/user-settings.git
    cd user-settings
    touch README.md
    git add README.md
    git commit -m "add README"
    git push -u origin master

## Mevcut Klasör

    cd existing_folder
    git init
    git remote add origin git@gitlab.com:enes.demir/user-settings.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master
## Mevcut Git Deposu

    cd existing_repo
    git remote rename origin old-origin
    git remote add origin git@gitlab.com:enes.demir/user-settings.git
    git push -u origin --all
    git push -u origin --tags

